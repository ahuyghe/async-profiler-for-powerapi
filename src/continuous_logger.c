#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//for open
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
//for mutex
#include<pthread.h>

#include "formated_buf.h"
#include "continuous_logger.h"
#include "shared_data.h"

static void
delete_file (const char *file)
{
  if (access (file, F_OK) != -1)
    {
      // file exists
      if (remove (file) != 0)
	perror ("profiler could not erase file");
    }
  return;
}

void *
writer_routine (void *vdata)
{

  struct writer_data *data = vdata;
  int fd;

  if (s_data.output_file == NULL)
    {
      delete_file (DEFAULT_CSV_FILE);
      if ((fd =
	   open (DEFAULT_CSV_FILE, O_CREAT | O_WRONLY | O_APPEND,
		 S_IRWXU)) == -1)
	perror ("open");
    }
  else
    {
      delete_file (s_data.output_file);
      if ((fd =
	   open (s_data.output_file, O_CREAT | O_WRONLY | O_APPEND,
		 S_IRWXU)) == -1)
	perror ("open");
    }

  while (*data->isRunning || (data->formated_result->offset != 0))
    {

      usleep (s_data.interval << 4);//based on empiric result (one trace is often arround 500bytes)
      
      //getting as close as an offset of 4096, without getting over it, so a single block is being written.
      //Traces should not be higher than 500
      if (data->formated_result->offset > 3500)
	{
	  pthread_mutex_lock(&s_data.lock);
       
	  write (fd, data->formated_result->buf,
	  	 data->formated_result->offset);
	  data->formated_result->offset = 0;

	  pthread_mutex_unlock(&s_data.lock); 
	} 

    }
  close (fd);
  free (data->formated_result);
  free (data);
  pthread_mutex_destroy(&s_data.lock); 
  return NULL;
}
