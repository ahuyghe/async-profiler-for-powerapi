#include <unistd.h>		//for sleep
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>

#include "shared_data.h"
#include "start_profiler.h"
#include "options.h"

int *isRunning, *hasFinished;

//char* thread_name;

void
handle_jvmti_error (jvmtiError error)
{
  if (error != JVMTI_ERROR_NONE)
    {
      printf ("agent error occured, see %d", error);
      exit (EXIT_FAILURE);
    }
}

// Calls GetClassMethods on a given class to force the creation of
// jmethodIDs of it.
void
CreateJMethodIDsForClass (jvmtiEnv * jvmti_env, jclass klass)
{
  jint method_count;
  jmethodID *methods_id;
  jvmtiError error;

  error =
    (*jvmti_env)->GetClassMethods (jvmti_env, klass, &method_count,
				   &methods_id);
  // Some class are not prepared but will be catch in vmInit
  if (error != JVMTI_ERROR_NONE && error != JVMTI_ERROR_CLASS_NOT_PREPARED)
    {
      handle_jvmti_error (error);
    }

  return;
}

void JNICALL
onClassPrepare (jvmtiEnv * jvmti_env, JNIEnv * jni_env, jthread thread,
		jclass klass)
{
  // make sure that all of the methodIDs 
  // have been initialized internally for 
  // AsyncGetCallTrace. 
  CreateJMethodIDsForClass (jvmti_env, klass);

}

static void JNICALL
compiledMethodLoad (jvmtiEnv * jvmti, jmethodID method,
		    jint code_size, const void *code_addr,
		    jint map_length, const jvmtiAddrLocationMap * map,
		    const void *compile_info)
{
  // Needed to enable DebugNonSafepoints info by default
}

void JNICALL
vmInit (jvmtiEnv * jvmti_env, JNIEnv * jni_env, jthread thread)
{
  jint class_count;
  jclass *klasses;
  jvmtiError error;
  int i;

  error = (*jvmti_env)->GetLoadedClasses (jvmti_env, &class_count, &klasses);
  handle_jvmti_error (error);

  for (i = 0; i < class_count; i++)
    CreateJMethodIDsForClass (jvmti_env, klasses[i]);

  return;
}

/*
 * AGCT need class loading events to be load;
 */
void JNICALL
classLoad (jvmtiEnv * jvmti_env, JNIEnv * jni_env, jthread thread,
	   jclass klass)
{
}

/*
 * Create a new java.lang.Thread
 */
jthread
create_java_thread (JNIEnv * env)
{
  jclass thrClass;
  jmethodID cid;
  jthread res;

  thrClass = (*env)->FindClass (env, "java/lang/Thread");
  if (thrClass == NULL)
    {
      perror ("Cannot find Thread class\n");
    }
  cid = (*env)->GetMethodID (env, thrClass, "<init>", "()V");
  if (cid == NULL)
    {
      perror ("Cannot find Thread constructor method\n");
    }
  res = (*env)->NewObject (env, thrClass, cid);
  if (res == NULL)
    {
      perror ("Cannot create new Thread object\n");
    }

  return res;
}

void JNICALL
threadStart (jvmtiEnv * jvmti_env, JNIEnv * jni_env, jthread thread)
{
  static int main_started = 0;

  if (!main_started)
    {

      //todo : check if it's the main thread
      //printf("thread start\n");
      jvmtiThreadInfo thread_info;
      int error;
      sigset_t prof_signal_mask;

      sigaddset (&prof_signal_mask, SIGPROF);
      pthread_sigmask (SIG_UNBLOCK, &prof_signal_mask, NULL);

      error = (*jvmti_env)->GetThreadInfo (jvmti_env, thread, &thread_info);
      if (error == JNI_OK)
	{
	  if (strcmp (thread_info.name, "main") == 0)
	    {
	      main_started = 1;

	      //Java Application started: start profiling     
	      {
		struct profiler_data *data;
		data = (struct profiler_data *)
		  malloc (sizeof (struct profiler_data));
		data->jvmti = jvmti_env;
		isRunning = malloc (sizeof (int));
		hasFinished = malloc (sizeof (int));
		*isRunning = 1;
		*hasFinished = 0;
		data->isRunning = isRunning;
		data->hasFinished = hasFinished;
		jthread profiler_thread = create_java_thread (jni_env);

		(*jvmti_env)->RunAgentThread (jvmti_env,
					      profiler_thread,
					      profiler_start,
					      data,
					      JVMTI_THREAD_MIN_PRIORITY);
	      }
	    }
	}
    }
}

JNIEXPORT void JNICALL
onVmDeath (jvmtiEnv *jvmti_env,
	    JNIEnv* jni_env
	   )
{
  //stop profiling thread...
  if (isRunning != NULL){
    *isRunning = 0;
  }
  
  //waiting profiling thread to finish
  while (*hasFinished <= 0)	
    {
      sleep (1);	
    }

  return;	
}


JNIEXPORT jint JNICALL
Agent_OnLoad (JavaVM * jvm, char *options, void *reserved)
{
  jvmtiCapabilities caps;	//change jvmti functionnality
  jvmtiEventCallbacks cb;	//wrap function to specific event
  jvmtiEnv *jvmti = NULL;
  jvmtiError error;

  parse_options (options);

  s_data.g_jvm = (const _Atomic (JavaVM) *) jvm;

  /*
   * Get JVMTI interface on the current version
   */
  error = (*jvm)->GetEnv (jvm, (void **) &jvmti, JVMTI_VERSION);
  handle_jvmti_error (error);

  /*
   * acquire capability
   */
  memset (&caps, 0, sizeof (caps));
  caps.can_generate_compiled_method_load_events = 1;
  error = (*jvmti)->AddCapabilities (jvmti, &caps);
  handle_jvmti_error (error);

  /*
   * match events to functions...
   */
  memset (&cb, 0, sizeof (cb));
  
  cb.VMInit = vmInit;
  cb.ClassLoad = classLoad;
  cb.CompiledMethodLoad = compiledMethodLoad;
  cb.ClassPrepare = onClassPrepare;
  cb.ThreadStart = threadStart;
  cb.VMDeath = onVmDeath;
  /*
   * ...set event callbacks...
   */
  error = (*jvmti)->SetEventCallbacks (jvmti, &cb, sizeof (cb));
  handle_jvmti_error (error);
  /*
   * ...and enable them
   */
  {
    jvmtiEvent events[] = { JVMTI_EVENT_VM_INIT, JVMTI_EVENT_COMPILED_METHOD_LOAD,
			    JVMTI_EVENT_CLASS_LOAD, JVMTI_EVENT_CLASS_PREPARE,
			    JVMTI_EVENT_THREAD_START, JVMTI_EVENT_THREAD_END,
			    JVMTI_EVENT_VM_DEATH };
    int i;
    size_t num_events = sizeof (events) / sizeof (jvmtiEvent);

    for (i = 0; i < num_events; i++)
      {
	error =
	  (*jvmti)->SetEventNotificationMode (jvmti,
					      JVMTI_ENABLE, events[i], NULL);
	handle_jvmti_error (error);
      }
  }

  {				//BLOCK profiling signal on this thread
    sigset_t prof_signal_mask;

    sigaddset (&prof_signal_mask, SIGPROF);
    pthread_sigmask (SIG_BLOCK, &prof_signal_mask, NULL);

  }

  return JNI_OK;
}
