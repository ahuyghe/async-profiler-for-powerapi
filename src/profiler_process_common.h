#include "formated_buf.h"

typedef struct read_buf
{

  int offset;
  char buf[PIPE_BUF];

} read_buf;

typedef struct method_data
{

  jmethodID id;
  int is_compiled;
  int error;
  char *name, *signature, *generic;

} method_data;

void record_name (formated_buf * f_buf, method_data * m_data, char del);
void record_signature (formated_buf * f_buf, method_data * m_data, char del);
void record_unknown (formated_buf * f_buf, char del);
char *get_error_name (int error);

int read_int_from_buffer (read_buf * r_buf);
__uint64_t read_uint64_from_buffer (read_buf * r_buf);
int fill_method_data (read_buf * r_buf, method_data * m_data);
int get_info_from_jvmti (jvmtiEnv * jvmti, formated_buf * f_buf,
			 method_data * m_data, char sig_del, char name_del);

void write_in_formated_buffer (formated_buf * f_buf, char *array_to_write,
			       int array_size, char del);
