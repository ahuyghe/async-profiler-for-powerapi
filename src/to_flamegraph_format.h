#define SIG_DEL '.'		//delimiter between signature and method name
#define MET_DEL ';'		//delimiter between method of the same trace

void print_all_table (gpointer key, gpointer value, gpointer user_data);
void extract_formated_trace_from_buffer (formated_buf * f_buf, char **trace);
void insert_a_trace_in_the_table (char **trace, GHashTable * table);
