#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "memory_getter.h"

/*
 * Quickly guesses where the field is_compiled is.
 * The value can only be between 0 and 4. fields 
 * obtaining something out of those values are excluded
 * from the research.
 *
 * JIT will profile a method to 3, 
 * then C2 will quickly compile it to 4
 * 
 * The method is searching an int wich can take value 3 and 4
 * The first offset found corresponding this criteria should be correct,
 * unless very specific case.
 * see: http://hg.openjdk.java.net/jdk/jdk12/file/06222165c35f/src/hotspot/share/runtime/tieredThresholdPolicy.cpp#l687
 * More strict criteria could be done, as searching 1, 2, 3, 4
 * instead of just 3 and 4. 
 * But this will result in a slower start up for profiling. 
 * (it's very rare that modern hotspots want to compile at level 1 and 2)
 *  
 * Since CodeCache is never at the bordure of the program's memory, 
 * there is no chance of having segfault under this method.
 */
static int
get_is_compiled_offset (const int *nmethod_pointer)
{

  static int is_compiled_offset = 0;
  static int offset_candidates[90] = { 0 };

  int i;

  //offset already found
  if (is_compiled_offset)
    {
      return is_compiled_offset;
    }

  for (i = 0; i < 90; i++)
    {
      if (offset_candidates[i] >= 0
	  && nmethod_pointer[i] >= 0 && nmethod_pointer[i] <= 4)
	{

	  switch (nmethod_pointer[i])
	    {
	    case 3:
	      offset_candidates[i] |= 1;
	      break;
	    case 4:
	      offset_candidates[i] |= 2;
	      break;
	    default:
	      break;
	    }

	  if (offset_candidates[i] == 3)
	    {			//value found
	      is_compiled_offset = i;
	      return is_compiled_offset;
	    }

	}
      else
	offset_candidates[i] = -1;	//Value outside of [0-4], exclude it
    }

  return -1;
}

/*
 * test the pointer to the compiled code from the jmethodID
 *
 * Maybe the offset isn't 8 on all configuratoin  (compilers/binary/32 64 depending)
 * see http://hg.openjdk.java.net/jdk8u/jdk8u/hotspot/file/9989538b7507/src/share/vm/oops/method.hpp#l79
 *
 * if compiled, get the compiled_level
 * see http://hg.openjdk.java.net/jdk/jdk/file/80b27dc96ca3/src/hotspot/share/code/nmethod.hpp#l114
 */
// java 1.8 nmethod = 9, compiled_level offset 52 (26)
int
get_is_compiled (const void *opaque_pointer)
{

  void *nmethod_pointer;
  int is_compiled_offset = -1;

  nmethod_pointer = *((*(void ***) opaque_pointer) + 8);

  if (nmethod_pointer != NULL)
    {

      is_compiled_offset =
	get_is_compiled_offset ((const int *) nmethod_pointer);

      if (is_compiled_offset != -1)
	{
	  int is_compiled = ((int *) (nmethod_pointer))[is_compiled_offset];
	  return is_compiled;
	}
      else
	return 0;
    }
  else
    return 0;
}
