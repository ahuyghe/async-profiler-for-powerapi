#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <gmodule.h>

#include "shared_data.h"
#include "profiler_process_common.h"
#include "to_flamegraph_format.h"

void
extract_formated_trace_from_buffer (formated_buf * f_buf, char **trace)
{

  *trace = malloc (f_buf->offset + 1);

  if (*trace == NULL)
    {

      perror ("profiler has stopped : malloc error");
      pthread_exit (NULL);

    }

  memcpy (*trace, f_buf->buf, f_buf->offset);
  (*trace)[f_buf->offset] = '\0';

  return;
}

void
insert_a_trace_in_the_table (char **trace, GHashTable * table)
{

  int *last_value = NULL;

  if ((last_value = (int *) (g_hash_table_lookup (table, *trace))) != NULL)
    {

      (*last_value)++;
      free (*trace);

    }
  else
    {

      int *new_value;
      new_value = malloc (sizeof (int));

      *new_value = 1;
      g_hash_table_insert (table, *trace, new_value);

    }
  return;
}

void
print_all_table (gpointer key, gpointer value, gpointer user_data)
{
  int size_of_buf, fd;
  char *buf;

  fd = *((int *) user_data);
  //printf("%d\n", fd);

  size_of_buf = strlen ((char *) key) + 10 + 3;
  buf = malloc (size_of_buf);
  memset (buf, '\0', size_of_buf);

  sprintf (buf, "%s %d\n", (char *) key, *(int *) (value));
  size_of_buf = strlen (buf);

  write (fd, buf, size_of_buf);

  free (key);
  free (value);

  return;
}
