#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>		//for perror
#include <glib.h>
//for mutex
#include<pthread.h>

#include "profiler.h"
#include "memory_getter.h"

#define ERROR_ALLOC(i,n)  error_name = malloc (i); memcpy (error_name, n, i);

/*
 * get jmethodid and compiled_level
 */
int
fill_method_data (read_buf * r_buf, method_data * m_data)
{
  m_data->id = *((jmethodID *) (r_buf->buf + r_buf->offset));
  r_buf->offset += sizeof (jmethodID);

  if (s_data.options_flag & JIT_LEVEL)
    {
      if (m_data->id == NULL)
	m_data->is_compiled = 0;
      else
	m_data->is_compiled = get_is_compiled (m_data->id);
    }
  else
    m_data->is_compiled = -1;

  return 1;
}

int
read_int_from_buffer (read_buf * r_buf)
{

  int result;

  result = *((int *) (r_buf->buf + r_buf->offset));
  r_buf->offset += sizeof (int);

  return result;
}

__uint64_t
read_uint64_from_buffer (read_buf * r_buf)
{

  __uint64_t result;

  result = *((__uint64_t *) (r_buf->buf + r_buf->offset));
  r_buf->offset += sizeof (__uint64_t);

  return result;
}

/*
 * from the JmethodID and by calling JVMTI functions, 
 * get name and signature of the method
 * 
 * if no errors happened, record everything in formated_buf
 */
int
get_info_from_jvmti (jvmtiEnv * jvmti, formated_buf * f_buf,
		     method_data * m_data, char sig_del, char name_del)
{
  jvmtiError error;

  error = (*jvmti)->GetMethodName (jvmti,
				   m_data->id,
				   &m_data->name, &m_data->signature, NULL);

  if (error == JVMTI_ERROR_NONE)
    {

      record_signature (f_buf, m_data, sig_del);
      (*jvmti)->Deallocate (jvmti, (unsigned char *) m_data->signature);

      record_name (f_buf, m_data, name_del);
      (*jvmti)->Deallocate (jvmti, (unsigned char *) m_data->name);

    }
  else
    {
      perror ("agent: couldn't get method name");
      free (f_buf->buf);
      return -1;
    }

  return 1;
}

/*
 * Somewhere an error occured.
 * The method name is lost
 */
void
record_unknown (formated_buf * f_buf, char del)
{

  method_data unknown_method;

  unknown_method.name = "<unknown>";
  unknown_method.is_compiled = -1;

  record_name (f_buf, &unknown_method, del);

  return;
}

/*
 * signature looks like this :"(III)Llib/something;" 
 * Function removes the first brackets, the return type "L", it's content,
 * and the final ';'.
 * The Stack trace includes the parameters types inside 
 * brackets wich has no interest for flamegraphs
 *
 * new_string : new allocated string without brackets
 * signature : signature holding the brackets 
 * return => the size of the newly "next_string" allocated string
 */
int
remove_bracket (char **new_string, const char *signature)
{
  char *new_pos;
  int new_size;

  new_pos = strchr (signature, ')') + 2;	//we eat ')' and 'L' => +2
  if ((new_pos - signature) >= strlen (signature) || new_pos == NULL)
    {
      /*
       * this signature probably looks like (null) or ()L
       */
      return -1;
    }
  else
    {

      new_size = strlen (new_pos) - 1;	//we do not need the final '\0'

      if (new_size <= 0)
	{
	  /*
	   * this should not happen,
	   * but if it does, no memory is allocated
	   * and function ends
	   */
	  *new_string = NULL;
	  return new_size;

	}

      else
	{

	  *new_string = malloc (new_size);
	  if (new_string == NULL)
	    {
	      perror ("malloc");
	      return -1;
	    }

	  memcpy (*new_string, new_pos, new_size);

	}
    }

  return new_size;
}

/*
 * Increase size of buffer if necessary
 *
 * formated_buf has a limited size, we need to make sure nothing is written beyond his boundary 
 */
static void
check_buffer_size (formated_buf * f_buf, int additionnal_offset)
{

  if ((f_buf->offset + additionnal_offset) > f_buf->size)
    {

      int old_size;

      old_size = f_buf->size;
      f_buf->size = old_size * 2;
      f_buf->buf = realloc (f_buf->buf, f_buf->size);

      if (f_buf->buf == NULL)
	{
	  perror ("profiler has stopped : realloc error");
	  pthread_exit (NULL);
	}

      memset (f_buf->buf + old_size, '\0', old_size);
    }

  return;
}

/*
 * Record an information and his delimiter.
 * 
 * if only a delimiter is wished to be write, 
 * put array_size to 0 and array_to_write to NULL
 */
void
write_in_formated_buffer (formated_buf * f_buf, char *array_to_write,
			  int array_size, char del)
{
  pthread_mutex_lock(&s_data.lock); 
  check_buffer_size (f_buf, array_size);
  
  memcpy (f_buf->buf + f_buf->offset, array_to_write, array_size);
  f_buf->offset += array_size;

  f_buf->buf[f_buf->offset] = del;
  f_buf->offset++;
  pthread_mutex_unlock(&s_data.lock); 

  return;
}

void
record_signature (formated_buf * f_buf, method_data * m_data, char del)
{

  char *formated_signature;
  int signature_size;
  //create formated_signature
  signature_size = remove_bracket (&formated_signature, m_data->signature);

  if (signature_size > 0)
    {
      write_in_formated_buffer (f_buf, formated_signature,
				signature_size, del);
    }

  return;
}

/*
 * Get name size and copy the name inside the buffer
 * Print token depending on the interpretation/compilation level
 * This enable a different color maching when using flamegraph
 */
void
record_name (formated_buf * f_buf, method_data * m_data, char del)
{

  int name_size = -1;

  name_size = strlen (m_data->name);

  if (name_size > 0)
    {

      write_in_formated_buffer (f_buf, m_data->name, name_size, '_');

      switch (m_data->is_compiled)
	{

	case (0):
	  write_in_formated_buffer (f_buf, ("[i]"), 3, del);
	  break;

	case (1):
	  write_in_formated_buffer (f_buf, ("[j1]"), 4, del);
	  break;

	case (2):
	  write_in_formated_buffer (f_buf, ("[j2]"), 4, del);
	  break;

	case (3):
	  write_in_formated_buffer (f_buf, ("[j3]"), 4, del);
	  break;

	case (4):
	  write_in_formated_buffer (f_buf, ("[j4]"), 4, del);
	  break;

	default:
	  f_buf->offset--;	//removing the delimiter  '_' added before the switch
	  f_buf->buf[f_buf->offset] = del;
	  f_buf->offset++;

	  break;
	}
    }
  return;
}

char *
get_error_name (int error)
{
  char *error_name;

  switch (error)
    {

    case (ticks_no_Java_frame):
      ERROR_ALLOC (14, "no_java_frame");
      break;

    case (ticks_no_class_load):
      ERROR_ALLOC (14, "no_class_load");
      break;

    case (ticks_GC_active):
      ERROR_ALLOC (10, "gc_active");
      break;

    case (ticks_not_walkable_not_Java):
      ERROR_ALLOC (9, "not_java");
      break;

    case (ticks_unknown_not_Java):
      ERROR_ALLOC (18, "not_java(unknown)");
      break;

    case (ticks_unknown_Java):
      ERROR_ALLOC (13, "unknown_java");
      break;

    case (ticks_not_walkable_Java):
      ERROR_ALLOC (18, "not_walkable_java");
      break;

    case (ticks_unknown_state):
      ERROR_ALLOC (14, "unknown state");
      break;

    case (ticks_thread_exit):
      ERROR_ALLOC (12, "thread exit");
      break;

    case (ticks_deopt):
      ERROR_ALLOC (10, "jit deopt");
      break;

    case (ticks_safepoint):
      ERROR_ALLOC (10, "safepoint");
      break;

    case (ticks_skipped):
      ERROR_ALLOC (8, "skipped");
      break;
    case (-12):
      ERROR_ALLOC (15, "profiler_error");
    default:
      return NULL;
    }

  return error_name;
}
