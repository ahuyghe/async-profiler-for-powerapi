void profiler_start (jvmtiEnv * jvmti, JNIEnv * jni_env, void *data);

struct profiler_data
{

  jvmtiEnv *jvmti;
  int *isRunning;
  int *hasFinished;
};
