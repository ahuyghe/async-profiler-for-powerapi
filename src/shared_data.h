#include "jvm.h"

#define DEFAULT_OUTPUT_FILE "sampling.result"
#define DEFAULT_CSV_FILE "sampling.csv"

enum options_flag_values
{ JIT_LEVEL = 0x01, SMART_WATTS = 0x02, CONTINUOUS_LOGGING = 0x04
};

/*
 * Global data.
 * Data use in SigHandler have been 
 * rendered atomic. 
 * Do not modify the value of any those 
 * variable outside Agent_Onload if
 * used in sighandler
 */

typedef struct shared_data
{

  /*
   * lowest bit(0x01) is a boolean for jit_level
   * secont bit(0x02) is a boolean for do_not_check_for_sigsegv
   * other options have not been decided yet
   */
  char options_flag;
  pthread_mutex_t lock; 
  char *output_file;
  int interval, start_time, max_frame_number;
    jvmtiError (*_asyncGetCallTrace) (ASGCT_CallTrace * trace, jint depth,
				      void *uncontext);
  const _Atomic (int) pipefd[2];
  const _Atomic (JavaVM) * g_jvm;

} shared_data;

shared_data s_data;
