#define _GNU_SOURCE

#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ucontext.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>
#include <x86intrin.h>
#include <inttypes.h>

#include "shared_data.h"	// for options_flag, pipefd[1]n g_jvm
#include "max_frame_number.h"

static void
cpy_int (_Atomic (char) * buf, _Atomic (int) * offset, int value)
{

  memcpy (buf + *offset, &value, sizeof (int));
  *offset += sizeof (int);

  return;
}

static void
cpy_uint64 (_Atomic (char) * buf, _Atomic (int) * offset, __uint64_t uint64)
{

  memcpy (buf + *offset, &uint64, sizeof (__uint64_t));
  *offset += sizeof (__uint64_t);

}

static void
cpy_pointer (_Atomic (char) * buf, _Atomic (int) * offset, void *value)
{

  memcpy (buf + *offset, &value, sizeof (void *));
  *offset += sizeof (void *);

  return;
}

/*
 * Send num_frame and methodid from the trace to the pipe
 * PIPE_BUF should be enough if MAX_FRAME_NUMBER is set correctly 
 */
static void
log_trace (ASGCT_CallTrace trace)
{

  int frame_number;

  _Atomic (char) temp_buf[PIPE_BUF] = { 0 };
  _Atomic (int) total_wrote = 0;

  cpy_int (temp_buf, &total_wrote, trace.num_frames);

  for (frame_number = trace.num_frames - 1; frame_number >= 0; frame_number--)
    cpy_pointer (temp_buf, &total_wrote,
		 trace.frames[frame_number].method_id);

  write (s_data.pipefd[1], temp_buf, total_wrote);

  return;
}

void
sigprof_handler (int signal, siginfo_t * info, void *ucontext)
{
  JNIEnv *jni_env;

  ASGCT_CallTrace trace;
  ASGCT_CallFrame storage[MAX_FRAME_NUMBER] = { 0 };
  int getEnvStat;
  int errno_saved;

  errno_saved = errno;

  getEnvStat =
    (*s_data.g_jvm)->GetEnv ((JavaVM *) s_data.g_jvm, (void **) &jni_env,
			     JNI_VERSION_1_6);

  if (getEnvStat == JNI_EDETACHED)
    {
      errno = errno_saved;
      return;
    }

  trace.env = jni_env;
  trace.frames = storage;
  (*s_data._asyncGetCallTrace) (&trace, MAX_FRAME_NUMBER, ucontext);

  if (trace.num_frames == ticks_unknown_Java)
    {
      //SEE NOTE

    }

  log_trace (trace);
  errno = errno_saved;

  return;
}

/*
 * Send num_frame and methodid from the trace to the pipe
 * PIPE_BUF should be enough if MAX_FRAME_NUMBER is set correctly 
 */
static void
log_trace_rdtsc (ASGCT_CallTrace trace, __uint64_t rdtsc_value)
{

  int frame_number;

  _Atomic (char) temp_buf[PIPE_BUF] = { 0 };
  _Atomic (int) total_wrote = 0;

  cpy_uint64 (temp_buf, &total_wrote, rdtsc_value);
  cpy_int (temp_buf, &total_wrote, trace.num_frames);

  for (frame_number = trace.num_frames - 1; frame_number >= 0; frame_number--)
    cpy_pointer (temp_buf, &total_wrote,
		 trace.frames[frame_number].method_id);

  write (s_data.pipefd[1], temp_buf, total_wrote);

  return;
}

void
sigprof_handler_rdtsc (int signal, siginfo_t * info, void *ucontext)
{
  JNIEnv *jni_env;

  ASGCT_CallTrace trace;
  ASGCT_CallFrame storage[MAX_FRAME_NUMBER] = { 0 };
  int getEnvStat;
  int errno_saved;
  __uint64_t rdtsc_value;

  errno_saved = errno;
  rdtsc_value = __rdtsc ();
  getEnvStat =
    (*s_data.g_jvm)->GetEnv ((JavaVM *) s_data.g_jvm, (void **) &jni_env,
			     JNI_VERSION_1_6);

  if (getEnvStat == JNI_EDETACHED)
    {
      errno = errno_saved;
      return;
    }

  trace.env = jni_env;
  trace.frames = storage;

  if (s_data.max_frame_number == 0)
    (*s_data._asyncGetCallTrace) (&trace, MAX_FRAME_NUMBER, ucontext);
  else
    (*s_data._asyncGetCallTrace) (&trace, s_data.max_frame_number, ucontext);

  log_trace_rdtsc (trace, rdtsc_value);
  errno = errno_saved;

  return;
}

//NOTE :
/* If current Java stack is not walkable (e.g. the top frame is not fully constructed),
     * try to manually pop the top frame off, hoping that the previous frame is walkable.
     * This is a temporary workaround for AsyncGetCallTrace issues,
     * see https://bugs.openjdk.java.net/browse/JDK-8178287
     *
     * workaround from
     * https://github.com/jvm-profiling-tools/async-profiler/blob/master/src/profiler.cpp#L263
     */

    /*
     * from ucontext_t structure, we can get the CPU register with :
     * pc : program counter (holds the address of the current instruction)
     * sp : stack pointer (holds functions, pointing the top. it's decreasing when pushed, and increase when poped).
     * fp : frame pointer : hold sp's value before a function is called
     *
     * context => register
     */

    /*
     * Step 1: Guess the top method frame using PC, manually create the call trace.
     We need to identify the border of each blob inside code cache,
     then compare the value of PC with those borders.
     From the blob, get the jmethodID

     * Step 2: Pop the last frame. Need some security to not make ASGCT crash.
     Change value of pc, sp, and fp correctly.
     Then check if the new pc is native code or codecache.
     If it isn't, skip to step 4.

     * Step 3:
     We got the tip of the frame, we now need to get the rest.
     Call ASCGT like we normally would with "MAX_FRAME_NUMBER -1".

     * Step 4: Restore the context.
     */

    /*
     * What's missing right now :
     We could get an approximation of the code cache borders since
     it's already getting the compiled method.
     But it doesn't not know nor the border of each blob (yet), neither the native library.
     */
