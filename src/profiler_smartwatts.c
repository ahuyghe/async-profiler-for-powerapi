#include <glib.h>		// for ghashtable
#include <unistd.h>		//for close
#include <string.h>

//for open
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//for rdtsc
#include <inttypes.h>

#include "profiler.h"

#define SIG_DEL '.'
#define MET_DEL ' '

/*
 * write csv header
 */
void
profiler_write_header (formated_buf * formated_result)
{
  write_in_formated_buffer (formated_result, "timestamp;callstack", 19, '\n');
  return;
}

void
profiler_smartwatts_process (read_buf * r_buf, formated_buf * formated_result,
			     jvmtiEnv * jvmti)
{

  method_data m_data;		//hold current method_data
  int num_frames = 0, index_frame = -1;

  {				//Get RDTSC and record it 
    __uint64_t rdtsc_value;
    char rdtsc_as_array[19] = { 0 };
    int rdtsc_as_array_size;
    rdtsc_value = read_uint64_from_buffer (r_buf);
    rdtsc_as_array_size = sprintf (rdtsc_as_array, "%" PRIu64, rdtsc_value);
    write_in_formated_buffer (formated_result, rdtsc_as_array,
			      rdtsc_as_array_size, ';');
  }

  init_method_data (&m_data);

  num_frames = read_int_from_buffer (r_buf);

  //printf("%d %d\n", index_frame, num_frames);
  if (num_frames < -11 || num_frames > (int) MAX_FRAME_NUMBER)
    {
      write_in_formated_buffer (formated_result, ("profiler erro"), 13, 'r');	//we used the delimiter as the last char of "profiler error" to avoid writing an actual delimiter
    }
  else if (num_frames > 0)
    {

      while (++index_frame < (num_frames))
	{

	  if (fill_method_data (r_buf, &m_data) == -1)
	    {
	      record_unknown (formated_result, MET_DEL);
	      break;
	    }

	  if (m_data.id != NULL)
	    {
	      if (!get_info_from_jvmti
		  (jvmti, formated_result, &m_data, SIG_DEL, MET_DEL))
		{
		  record_unknown (formated_result, MET_DEL);
		}
	    }
	  else
	    {
	      //No method id could be found : it should not have been created yet
	      record_unknown (formated_result, MET_DEL);
	    }

	}

    }
  else if (num_frames < 0)
    {

      char *error;
      int error_size;

      error = get_error_name (num_frames);
      error_size = strlen (error) + 1;

      write_in_formated_buffer (formated_result, error, error_size, '\n');
      free (error);
      return;
    }

  write_in_formated_buffer (formated_result, NULL, 0, '\n');

  return;
}

void
csv_dump (formated_buf * formated_result)
{

  stop_itimer ();

  int fd;

  if (s_data.output_file == NULL)
    fd = open (DEFAULT_CSV_FILE, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
  else
    fd = open (s_data.output_file, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
  if (fd == -1)
    perror ("agent error open");
  else
    {
      write (fd, formated_result->buf, formated_result->offset);
      close (fd);
    }

  return;
}
