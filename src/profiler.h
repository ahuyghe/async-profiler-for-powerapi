#include "shared_data.h"
#include "profiler_process_common.h"
#include "max_frame_number.h"

#include <x86intrin.h>

#define INITIAL_FORMATED_BUF_SIZE 4096

void handle_jvmti_error (jvmtiError error);
void write_int (char *buf, int *offset, int value);

void stop_itimer ();

void init_formated_buf (formated_buf * f_buf);
void init_method_data (method_data * m_data);

void profiler_count_end (GHashTable * table);
void profiler_count_process (read_buf * r_buf, GHashTable * table,
			     jvmtiEnv * jvmti);

void profiler_write_header (formated_buf * formated_result);
void profiler_smartwatts_process (read_buf * r_buf,
				  formated_buf * formated_result,
				  jvmtiEnv * jvmti);
void csv_dump (formated_buf * formated_result);
