#define _GNU_SOURCE 1
#define _POSIX_C_SOURCE 200809L

#include <unistd.h>
#include <dlfcn.h>
#include <sys/time.h>
#include <sys/epoll.h>
#include <poll.h>
#include <glib.h>
#include <errno.h>
#include <string.h>

#include "profiler.h"
#include "sigprof_handler.h"
#include "start_profiler.h"
#include "continuous_logger.h"

#define GENERATE_RANDOM_INTERVAL(i)  ( (i >> 1) + (random() % i) )

static struct sigaction old_sa;
int *isRunning = NULL, *hasFinished = NULL;

/*
 * For the agent thread to know when to exit.
 */
static void
end_profiler (int **isRunning, int *hasFinished)
{

  free (*isRunning);
  *isRunning = NULL;
  *hasFinished = 1;

  return;
}

static void
launch_writer (formated_buf ** formated_result)
{
  struct writer_data *data = malloc (sizeof (data));
  pthread_t thread;
  data->isRunning = isRunning;
  data->formated_result = *formated_result;

  if (pthread_create (&thread, NULL, writer_routine, data) != 0)
    perror ("pthread_create");

  return;
}

/*
 * Get the interval, and randomly changes his value between +50% or -50%
 */
long int
generate_random_interval ()
{
  return GENERATE_RANDOM_INTERVAL (s_data.interval);
}

static void
set_itimer ()
{
  struct itimerval timer;

  long int interval;

  interval = s_data.interval;

  timer.it_interval.tv_sec = 0;
  timer.it_interval.tv_usec = interval;

  if(s_data.start_time) {
    timer.it_value.tv_sec = s_data.start_time;
    timer.it_value.tv_usec = 0;
  }
  else {
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec = interval;  
  }
  
  if (setitimer (ITIMER_PROF, &timer, NULL) == -1)
    perror ("agent setitimer");

  return;
}

/*
 * generate a new random interval, then set it.
 */
static void
randomize_interval ()
{
  struct itimerval timer;

  long int generated_interval;
  generated_interval = generate_random_interval ();

  getitimer (ITIMER_PROF, &timer);
  timer.it_interval.tv_usec = generated_interval;

  if (setitimer (ITIMER_PROF, &timer, NULL) == -1)
    perror ("agent setitimer");

  return;
}

void
stop_itimer ()
{
  struct itimerval timer = { {0, 0}, {0, 0} };

  if (setitimer (ITIMER_PROF, &timer, NULL) == -1)
    perror ("agent stop setitimer");

  return;
}

/*
 * Set the signal handler that will give us our traces
 */
static void
set_signal_handler (void (*handler)
		    (int signal, siginfo_t * info, void *ucontext))
{
  struct sigaction sa;

  sa.sa_sigaction = handler;
  //SIGINFO will give the context to the sighandler as parameter
  sa.sa_flags = SA_SIGINFO;
  sigemptyset (&sa.sa_mask);

  if (sigaction (SIGPROF, &sa, NULL) == -1)
    {
      perror ("profiler agent sigaction");
      exit (0);
    }

  return;
}

/*
 * Catch the signal and end the profiler properly.
 * Then set the old signal handler, and raise again the signal caught
 * for the application to do its normal behavior
 */
void
added_handler (int signal, siginfo_t * info, void *ucontext)
{
  end_profiler (&isRunning, hasFinished);

  /* if(old_sa.sa_flags & SA_SIGINFO)  */
  /*   (*old_sa.sa_sigaction)(signal, info, ucontext); */
  /* else  */
  /*   (*old_sa.sa_handler)(signal); */
  if (sigaction (signal, &old_sa, NULL) == -1)
    {
      perror ("profiler agent sigaction");
      exit (0);
    }
  raise (signal);

  return;
}

/*
 * Add the added_handler. See added_handler
 */
static void
add_to_signal_handler (int signo)
{
  struct sigaction new_sa;

  new_sa.sa_sigaction = added_handler;
  sigemptyset (&new_sa.sa_mask);
  new_sa.sa_flags = SA_SIGINFO;

  if (sigaction (signo, &new_sa, &old_sa) == -1)
    {
      perror ("profiler agent sigaction");
      exit (0);
    }

  return;
}

/*
 * Set an epoll to start reading the data sent by the sighandler as soon as possible
 * Avoid doing a consuming loop, the thread will be sleeping until data's ready
 */
static int
set_epoll (struct epoll_event ev)
{
  int fdpoll;

  fdpoll = epoll_create (1);
  ev.data.fd = s_data.pipefd[0];
  ev.events = POLLIN;

  int retpoll = epoll_ctl (fdpoll, EPOLL_CTL_ADD, s_data.pipefd[0], &ev);
  if (retpoll == -1)
    {
      perror ("retpoll");
      pthread_exit (NULL);
    }

  return fdpoll;
}

/*
 * Get from the JVM the function that will get the traces.
 * The ASGCT_function must be called inside a signal handler
 */
static void
get_ASGCT_function ()
{
  *(void **) (&s_data._asyncGetCallTrace) =
    dlsym (RTLD_DEFAULT, "AsyncGetCallTrace");
  if (s_data._asyncGetCallTrace == NULL)
    {
      perror ("couldn't get AsyncGetCallTrace Function");
      pthread_exit (NULL);
    }

  return;
}

/*
 * Install the data the agent thread has gave us upon start
 */
static void
init_data (void **data, jvmtiEnv ** jvmti, int **isRunning, int **hasFinished)
{
  struct profiler_data *prof_data = *data;

  *isRunning = prof_data->isRunning;
  *hasFinished = prof_data->hasFinished;

  free (*data);
  *data = NULL;

  if(pthread_mutex_init(&s_data.lock, NULL) != 0) {
    perror("profiler mutex init");
  }
  return;
}

void
init_read_buf (read_buf * r_buf)
{

  r_buf->offset = 0;
  memset (r_buf->buf, '\0', PIPE_BUF);

  return;
}

void
init_formated_buf (formated_buf * f_buf)
{

  f_buf->offset = 0;
  f_buf->size = INITIAL_FORMATED_BUF_SIZE;
  f_buf->buf = malloc (INITIAL_FORMATED_BUF_SIZE);
  memset (f_buf->buf, '\0', INITIAL_FORMATED_BUF_SIZE);

  return;
}

void
init_method_data (method_data * m_data)
{

  m_data->name = NULL;
  m_data->signature = NULL;
  m_data->generic = NULL;

  return;
}

/*
 * Profiler main loop. won't stop until isRunning is true
 * Get data from the sighandler and call the appropriate
 * function to process the data
 *
 * void *result might be a simple char pointer if 
 * smart_watts option is on, or it can be a glib HashTable
 * for the default behavior
 */
static void
profiler_get_data (int *isRunning, int fdpoll, void *result,
		   jvmtiEnv * jvmti, void (*func_process) ())
{

  struct epoll_event events[1];
  int count = 0;
  int nfds;
  read_buf r_buf;		//carries raw method_id and flags get from sighandler

  while (*isRunning)
    {
      //printf("%d\n",*isRunning);
      /* 
       *  waits for the signal to happen, then start interpreting the data.
       *  it can start interpreting the data if "System Call Interrupted" error happens on epoll_wait,
       *  or directly if pipe got data before
       */
      if (count % 100 == 0)
	{
	  randomize_interval ();
	}

      init_read_buf (&r_buf);

      if ((nfds = epoll_wait (fdpoll, events, 1, 5)) > 0
	  || (nfds < 0 && errno == EINTR))
	{

	  if (errno == EINTR)
	    errno = 0;

	  count++;

	  if (read (s_data.pipefd[0], r_buf.buf, PIPE_BUF) > 0)
	    func_process (&r_buf, result, jvmti);
	  else
	    perror ("profiler read");

	}
      else if (nfds < 0)
	perror ("profiler epoll_wait");

    }				//while running > 0

  printf ("profiler end routine is starting with %d potential samples\n",
	  count);

  return;
}

/*
 * Called by the agent upon start, 
 * initialize any forms of environnement necessary for profiling, 
 * then start profiling.
 */
void
profiler_start (jvmtiEnv * jvmti, JNIEnv * jni_env, void *data)
{
  struct epoll_event ev;
  int fdpoll;

  if (s_data.options_flag & SMART_WATTS)
    set_signal_handler (sigprof_handler_rdtsc);
  else
    set_signal_handler (sigprof_handler);

  add_to_signal_handler (SIGINT);
  init_data (&data, &jvmti, &isRunning, &hasFinished);
  get_ASGCT_function ();

  //Set pipe  
  if (pipe ((int *) s_data.pipefd) == -1)
    {
      perror ("agent pipe error");
      exit (EXIT_FAILURE);
    }

  fdpoll = set_epoll (ev);
  set_itimer ();

  if (s_data.options_flag & SMART_WATTS)
    {

      formated_buf *formated_result = malloc (sizeof (formated_buf));

      init_formated_buf (formated_result);

      if (s_data.options_flag & CONTINUOUS_LOGGING)
	launch_writer (&formated_result);

      profiler_write_header (formated_result);
      profiler_get_data (isRunning, fdpoll, formated_result, jvmti,
			 profiler_smartwatts_process);

      if (!(s_data.options_flag & CONTINUOUS_LOGGING))
	{
	  csv_dump (formated_result);
	  free (formated_result);
	}

    }
  else
    {

      GHashTable *table;

      table = g_hash_table_new (g_str_hash, g_str_equal);

      profiler_get_data (isRunning, fdpoll, table, jvmti,
			 profiler_count_process);
      profiler_count_end (table);

    }

  end_profiler (&isRunning, hasFinished);
  return;
}
