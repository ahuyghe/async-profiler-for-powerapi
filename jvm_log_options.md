# JVM Log Options

### Print all flags on any JVM : 
```
java -XX:+UnlockDiagnosticVMOptions -XX:+PrintFlagsFinal -version
```
### Print all flags on a debug build JVM :
```
./jdk/build/linux-x86_64-server-fastdebug/jdk/bin/java -XX:+UnlockDiagnosticVMOptions -XX:+UnlockExperimentalVMOptions -XX:+PrintFlagsFinal -XX:+PrintFlagsWithComments -version
```
You can use grep false/Print/Log to only get options that are probably used for logs.

### To get start and end of a compilation with timestamps, on any JVM version, use :
```
java -XX:+UnlockDiagnosticVMOptions -XX:+PrintCompilation -XX:+PrintCompilation2
```
PrintCompilation 2 cannot be used without PrintCompilation.

### To get the "reason" of the compilation, on a debug version build, use
```
./jdk/build/linux-x86_64-server-fastdebug/jdk/bin/java -XX:+CIPrintRequests
```
see section "comment=" of each obtained line.