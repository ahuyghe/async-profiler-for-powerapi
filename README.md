## Async Profiler for PowerAPI

Async profiler for PowerAPI is a five month project as an intern inside 
Inria SPIRALS team. The goal is to provide a profiling tool for application 
running on a JVM. It records samples that can then be modelized in a flamegraph,
or records traces with timestamps to be use in addition with SmartWatts. 
The project is Linux specific with a x86 CPU and is intended to be use with 
openJDK 11 and +.

It was inspired by R.Warburton's honest profiler.
https://github.com/jvm-profiling-tools/honest-profiler

Most profilers use JVMTI GetCallTraceFunction wich can only be called during
safepoints. Safepoints happen on JVM heuristics to avoid calling GC and JIT
when the code is hot. This can be a huge bias for a sampling tool. 
The other frequent bias is calling the profiler on a regular interval, wich is 
problematic if the application is calling the same methods on the 
same periodicity.
More about profilers bias :
https://plv.colorado.edu/papers/mytkowicz-pldi10.pdf

Async profiler for PowerAPI avoids JVM safepoints bias by retrieving traces
asynchronously, periodicity bias by randomizing the intervals frequently, and
has a low overhead to avoid as much as possible the observer effect.

### Building

Kernel must be relatively recent (4.0 +).
Install libglib2.0-dev package and if necessary, add the path to the glib.h 
by modifying the Makefile. 
Simply use the command make inside the project folder.

### Running the agent

When launching the JVM, add the option -agentpath: followed by the 
relative path to bin/agent.so.

```
$ java -agentpath:./async-profiler-for-powerapi/bin/agent.so Foo
```
### Default behavior

The agent record a call trace sample at an interval of 15ms, and write every 
different traces followed by their count number inside sample.result file.
The format is as below. 

Signature are seperated from their method by a dot ".".
Methods are seperated by semicolons ";".
Count is given after a space. 

```
main;java/lang/Object.invoke;scala/collection/Seq.toSeq; 42

```

### Profiler options 

```
The following is the complete set of agent options :

file=XXXX            	      Record samples in the file specified by XXXX
interval=XXXX        	      Record each trace with an interval of XXXXμs.
			     Default value of 15ms (15 000μs). The interval will rarely be 
			     such a strict value. Instead, the profiler will often 
			     randomize the interval to at most 50% longer/shorter.
start_time=XXXX     	      Start profiling after XXXXs. Default value
			      is set to be equal to the interval
max_frame_number=XXXX         Record traces up to XXXX frames
smart_watts         	      Record traces with a timestamp instead of a count
continuous_logging	      write traces concurrently with the application.
			      Only for smart_watts. Includes an additional variable overhead. 
jit_level           	      Get each method jit compiled level [0-4]. 
                    	      0 being interpreted, 4 the highest compilation possible
                    	      and in-betweens being the level of profiling.
```

Options can be specifed by adding '=' after the path to agent.so.
```
$ java -agentpath:./async-profiler-for-powerapi/bin/agent.so=smart_watts Foo
```

Multiple options can be specified by seperating them with a ",".
```
$ java -agentpath:./async-profiler-for-powerapi/bin/agent.so=smart_watts,jit_level Foo
```

Some options must take a parameter, given after a '=' character
```
$ java -agentpath:./async-profiler-for-powerapi/bin/agent.so=file=samples_file Foo
```

### Print flamegraph

Use flamegraph.pl tool on the file containing the samples. 
Then, open in web browser.
```
$ async-profiler-for-powerapi/flamegraph.pl sampling.result > sampling.svg
$ firefox sampling.svg
```

If jit_level option was on, you can use the option "--color java" to color 
methods by their compiled level.

```
$ async-profiler-for-powerapi/flamegraph.pl --color java sampling.result > sampling.svg
$ firefox sampling.svg
```

For more information about flamegraph, see : 
https://github.com/brendangregg/FlameGraph

### Jit_level option

Print the level of compilation after each method under this format : "_[j1]", where 1 is the compilation level.
0 means that the compilation never occured i.e the method is still being interpreted from it's bytecode.
Under client mode or server mode, the compilation level stays the same (level 1 or  level 4), 
therefore the option can only be useful to understand whether the bytecode is being interpreted or not.
Jit_level is truely relevant when using the tiered-compilation, wich should be the default mode. 

More about level of compilation can be found here :
http://hg.openjdk.java.net/jdk/jdk12/file/06222165c35f/src/hotspot/share/runtime/tieredThresholdPolicy.cpp#l687

### Jit_level option warning

Jit_level blindly dig into method memory and can be dangerous to use on
jvm older than 11. Jit_level option might result on a systemic SIGSEGV, or 
no result at all depending on wich machine is using it.

### Smart_watts option


Smart_watts option write in sampling.csv by default.
It follows the format as below.

Timestamps and calltrace are sperated by a semicolons ";".
Methods and their signature are seperated by a dot ".".
Methods are seperated by a space " ".

```
timestamps;calltrace
18579887306874;run runWorker run java/lang/Object.run java/lang/Object.runTask
```
The timestamps is retrieve from rdtsc during the sighandler, just before getting 
the trace. For more information about rdtsc, see: 
https://en.wikipedia.org/wiki/Time_Stamp_Counter.

### Note about errors

The profiler gets a random active thread from JVM each time he's profiling.
Some threads like GC or JIT are not executing java code.
The JVM will sometimes gives errors instead of a trace, but those errors can still mean something.

```
gc_active    	        GC was doing a collection during his "stop-the-world" 
                        pause. No traces could be retrieved.
                        
jit_deopt               jit too makes a small "stop-the-world" to remove 
                        the compiled method.

not_java                A native function invoked with JNI is executing or JIT
                        is in the middle of compiling.

unknown_java	        Could be anything. The program counter wasn't in an 
                        explicit position enough for the JVM to get the called 
                        method.

not_walkable_java       Same as unknown java.

not_java(unknown)       It can't be the JIT neither can it be Java code.

profiler_error 	        When multiple signals happen while processing the 
                        data, corruption may occurs. Instead of continuing,
                        the profiler will record this error 
```